/* Drop */
drop database if exists scrum_meister;

create database if not exists scrum_meister;

use scrum_meister;

create table if not exists project (
    id int primary key auto_increment,
    name varchar(50) not null
);


insert into project (name) values ('Test project');