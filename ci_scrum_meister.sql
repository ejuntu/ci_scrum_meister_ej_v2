/* Drop */
/* OAMK Web-sovellusten arkkitehtuurit k. 2015
/* E. Juntura */
DROP DATABASE IF EXISTS scrum_meister;
CREATE DATABASE IF NOT EXISTS scrum_meister;
USE scrum_meister;

CREATE TABLE IF NOT EXISTS project (
id int primary key auto_increment,
name varchar(50) not null
);

CREATE TABLE IF NOT EXISTS sprint (
    id int primary key auto_increment,
    sprint_id varchar(10) not null,
    start date not null,
    end date not null,
    project_id int not null,
    CONSTRAINT fk_project_id foreign key (project_id) references project(id)
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS person (
    id int primary key auto_increment,
    name varchar(100) NOT NULL,
    email varchar(100) NOT NULL UNIQUE,
    password varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS project_person (
    id int primary key auto_increment,
    project_id int not null,
    CONSTRAINT fk_project_person_id foreign key (project_id) references project(id)
    ON DELETE RESTRICT,
    person_id int not null,
    CONSTRAINT fk_person_project_id foreign key (person_id) references person(id)
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS task (
    id int primary key auto_increment,
    project_id int not null,
    sprint_id varchar(10), 
    title varchar(50),
    description text,
    priority int,
    CONSTRAINT fk_project_task_id foreign key (project_id) references project(id)
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS task_person (
    id int primary key auto_increment,
    person_id int not null,
    CONSTRAINT fk_person_task_id foreign key (person_id) references person(id)
    ON DELETE RESTRICT,
    task_id int not null,
    CONSTRAINT fk_task_person_id foreign key (task_id) references task(id)
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS work (
    id int primary key auto_increment,
    duration FLOAT not null,
    description varchar(255) not null,
    person_id int,
    CONSTRAINT fk_person_work_id foreign key (person_id) references person(id)
    ON DELETE RESTRICT,
    task_id int not null,
    CONSTRAINT fk_task_work_id foreign key (task_id) references task(id)
    ON DELETE RESTRICT
);

create table if not exists activity (
    id int primary key auto_increment,
    message text not null,
    added timestamp default current_timestamp,
    project_id int not null,
    constraint fk_project_activity_id foreign key (project_id) references project(id)
    on delete restrict,
    person_id int not null,
    constraint fk_person_activity_id foreign key (person_id) references person(id)
    on delete restrict
);

INSERT INTO project (name) values('Test project');
INSERT INTO person (name, email, password) VALUES("Jaska Jokunen","jaska@jokunen.com",md5('testi'));
INSERT INTO person (name, email, password) VALUES("Hessu Hopo","hessu@hopo.com",md5('testi'));
/*INSERT INTO project_person (person_id,project_id) values (1,1);
INSERT INTO project_person (person_id,project_id) values (2,1);*/
INSERT INTO sprint (sprint_id, start, end, project_id) values("I","2015-03-20","2015-04-04",1);
INSERT INTO task (title, description, sprint_id, project_id ) VALUES("Planning","Plan the project",'I',1);
INSERT INTO task (title, description, sprint_id, project_id) VALUES("Vision document","Write vision document",'I',1);
/*INSERT INTO task_person(person_id,task_id) values(1,1);
INSERT INTO task_person(person_id,task_id) values(1,2);
INSERT INTO task_person(person_id,task_id) values(2,2);*/