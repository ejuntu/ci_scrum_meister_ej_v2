<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of activity_view
 *
 * @author Esa
 */
 ?>
<script>
    
    $(function() {
            //*********************** SAVE**************************
        $(".activity_textarea").keypress(function(e){
            if(e.which==13) {            
            $("#activity_insert").submit(); //submitataan muutokset
            }
        });
        $("a#delete").on("click", function(e) {
            var link = this;

            e.preventDefault();

            $("<div>Are you sure you want to delete message?</div>").dialog({
                buttons: {
                    "Ok": function() {
                        window.location = link.href;
                    },
                    "Cancel": function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
    
    });
    
</script>
    
<div id="new_activity">
<form id="activity_insert" action="<?php echo site_url().'activity/insert';?>" method="post">    
        <div>
            <textarea name="message" class="activity_textarea"
                placeholder="Add your message here, press enter to send">                          
            </textarea>

            <input type=hidden value="<?php print $user_id?>" name="user_id">
            <input type=hidden value="<?php print $project_id ?>" name="project_id">
        </div>
</form>    
</div>
        <?php
        $last_id=$activities[0]->id;
        print "<input id='last_id' type='hidden' value='".$last_id."' name='last_id'>";
        foreach ($activities as $activity) {            
            $person_id=$activity->person_id;
            print "<div id='act_message'>";
            //set current user name from $users
            foreach($users as $user){
                if ($user->id==$person_id) {
                    $user_name=$user->name;
                }
            };
            print "Added by ".$user_name."&nbsp";
            print "on ".$activity->added."&nbsp";
            print " <a id='delete' href='" .
                    site_url() . "activity/delete/" .  $activity->id . 
                    "'><span class='glyphicon glyphicon-trash'></span></a><br>";
            print "<div>".$activity->message;
            print "</div></div>";
        }
        ?>

