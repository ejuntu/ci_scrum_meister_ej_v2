<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */
?>
<script>
    $(function() {
        
        var oldTitle="";
        var oldDesc="";
        /*$("#add_task").hide();*/
        $("#hide_task").click(function(){
                $("#add_task").hide();
              });
        $("#show_task").click(function(){
                $("#add_task").show();
              });  
        //******************** EDIT TASK *********************************      
        $(".edit_task").click(function() {
           /* http://mrbool.com/how-to-add-edit-and-delete-rows-of-a-html-table-with-jquery/26721*/
           var par = $(this).parent().parent(); //tr rivi
           var tdId = par.children("td:nth-child(1)"); //id
           var tdTitle = par.children("td:nth-child(3)"); //kolmas td=title (eka on id, toka sprintti)
           var tdDesc = par.children("td:nth-child(4)"); //neljäs td=description, 5. Delete
           var tdSave = par.children("td:nth-child(6)"); //6. Save, 7. edit, 8. cancel
           var tdCancel = par.children("td:nth-child(8)"); 
           oldTitle=tdTitle.html();
           oldDesc=tdDesc.html();
           
           tdTitle.html("<input type='text' id='txtTitle' value='"+tdTitle.html()+"'/>"); 
           tdDesc.html("<input type='text' id='txtDesc' value='"+tdDesc.html()+"'/>"); 
           tdSave.html("<a class='save_task' href='#'>Save</a>");
           tdCancel.html("<a class='cancel_task' href='#'>Cancel</a>");
           $(".cancel_task").css('display','inline');
           $(".edit_task").css('display','none');
           
        });
        
        //*********************** SAVE**************************
        $(".save_task").click(function() {
            alert("Save clicked!");
            $("#task_form_update").submit(); //submitataan muutokset
        });
        
        
        //*********************  CANCEL *************************
        $(".cancel_task").click(function() {
           var par = $(this).parent().parent(); //tr rivi
           var tdTitle = par.children("td:nth-child(3)"); //kolmas td=r (eka on id, toka sprintti)
           var tdDesc = par.children("td:nth-child(4)"); //neljäs td=description, 5. Delete
           var tdSave = par.children("td:nth-child(6)"); //6. Save, 7. edit
           
           tdTitle.html(oldTitle); 
           tdDesc.html(oldDesc); 
           tdSave.html("");
           $(".edit_task").css('display','inline');
           $(".cancel_task").css('display','none');
           
        });
    });
</script>
<a href="#" id="show_task">Add new task</a>
<div id="add_task">
    <form action="<?php echo site_url().'backlog/save';?>" method="post">
        <div>
            <label>Title</label>
            <input name="title" maxlength="50">
        </div>
        <div>
            <label>Description</label>
            <textarea name="description"></textarea>
        </div>
        <input type="submit" value="Save">
        <a href="#" id="hide_task">Close</a>
    </form>
</div>
<form id="task_form_update" action="<?php print site_url()."backlog/update";?>" method="post">
    <table>
        <tr><th></th><th>Sprint</th><th>Task title</th><th>Task Description</th><th></th><th></th><th></th><th></th>
        </tr>
            <?php
            foreach ($tasks as $task) {
                print "<tr><td></td><td>&nbsp</td>&nbsp<td>$task->title</td>&nbsp<td>$task->description</td>";
                print "<td> <a onclick='return confirm(\"Delete task?\");' href='" . 
                        site_url() . "backlog/delete/" .  $task->id . "'>Delete</a>";
                print "<td/>";
                print "<td>";
                print "<a class='edit_task' href='#'>Edit</a>";
                print "</td>";
                print "<td>";
                print "<a class='cancel_task' href='#'></a>";
                print "</td>"; 
                print "</tr>";
            }
            ?>
    </table>
</form>

