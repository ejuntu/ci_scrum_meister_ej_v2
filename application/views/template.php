<!DOCTYPE html>
<!--
OAMK Web-arkkitehtuurit k. 2015
Esa Juntura
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $header?></title>
        <!-- Latest compiled and minified CSS -->
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type=text/css href="<?php echo base_url().'application/css/style.css' ?>"/>
        
        <script>
            
            
            $(function() {
            
              $( "#tabs" ).tabs({
                  active: <?php echo $active_tab;?>
              });
              
              $.ajaxSetup({ cache: false });
                         
              project_dialog=$("#project_dialog").dialog({
                  autoOpen: false,
                  height:175,
                  width:350,
                  modal:true                 
              });
              $("#add_project").click(function() {
                  project_dialog.dialog("open");
              });
              $("#close_project").click(function() {
                  project_dialog.dialog("close");
              });
              
              $( "#add_sprint").click(function() {
                sprint_dialog.dialog("open");
            });
            
            $( "#close_sprint").click(function() {
                sprint_dialog.dialog("close");
            });

            });
            
            function update() {
            //Luetaan viimeisen kentän id (last_id)
            last_id = $("#last_id").val();
            //alert(last_id);
            $.ajax({
                type:"POST",
                dataType:'json',
                url:"<?php print site_url();?>activity/update_feed",
                data: {"last_id":last_id},
                success: function(output) {                    
                    //alert($.param(output));
                    $.each(output,function(index,data) {
                        if(data.id>last_id){
                            last_id=data.id;
                        }
                        $('#last_id').before(
                            "<div id='act_message'>Added by "+
                            data.username+"&nbsp on"+data.added+
                            "&nbsp<a id='delete' href='<?php print site_url();?>"+
                             "activity/delete/"+data.id+
                             "'><span class='glyphicon glyphicon-trash'></span></a><br>"+
                            "<div>"+data.message+"</div></div>");
                                           
                    });
                    //Tähän last_id:n päivitys
                    $("#last_id").val(last_id); 
                }/*,
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }*/
            });
                    
            };
            setInterval(update,10000);
        </script>
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
                  data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
           <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    <?php 
                            if ($project!=NULL) {
                                print $project->name;
                            }
                            else {
                                print "No project";
                            }
                            ?>    
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                    <?php  if ($project!=NULL) {
                            foreach ($projects as $project) {
                                print "<li><a href='".site_url().'project/index/'.$active_tab.'/'.
                                    $project->id."'>".$project->name."</a></li>";
                            }
                        }?>
                      <li><a id="add_project" href="#">Add new...</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >
                    <?php 
                                if ($sprint!=NULL) {
                                    print $sprint->sprint_id; 
                                }
                                else {
                                    print "No sprints";
                                }
                                ?>
                    <span class="caret"></span></a>
                    <?php 
                                if ($sprint!=NULL) {
                                    print $sprint->sprint_id; 
                                }
                                else {
                                    print "No sprints";
                                }
                                ?>
                                
                                <span class="caret"></span>
                      <ul class="dropdown-menu" role="menu">
                      <?php
                                if ($sprint!=NULL) {
                                    foreach ($sprints as $sprint) {
                                        print "<li><a href='" . site_url() . 'project/change_sprint/' . $sprint->id  . "'>" .  $sprint->sprint_id  ."</a></li>";
                                    }
                                }
                                ?>
                                <li><a id="add_sprint" href="#">Add new sprint...</a></li>
                    </ul>
                </li>
                        <li class="active"><a href="#">
                    <?php if ($user!=NULL) {
                        print $user->name;
                    };?>
                    <span class="glyphicon glyphicon-user"></span></a></li>
              <li><a href="#"><span class="glyphicon glyphicon-cog"></span></a></li>
              <li><a href="<?php print site_url() . 'login/logout/';?>"
                      <span class="glyphicon glyphicon-off"></span></a></li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" style="margin-top: 100px;">
        <?php
            $data['active_tab']=$active_tab;
            $data['user']=$user;
            $this->load->view('project_view',$data);
            //$this->load->view('sprints_view',$data);
            //$this->load->view('persons_view',$data);
        ?>
        <div class="row">
            <div class="col-md-12">
                <div id="tabs">
                    <ul>
                        <li><a href="<?php echo base_url().'activity/';?>">Activity</a></li>
                        <li><a href="<?php echo base_url().'backlog/';?>">Backlog</a></li>
                        <li><a href="<?php echo base_url().'sprint/';?>">Sprint</a></li>
                        <li><a href="<?php echo base_url().'report/';?>">Reports</a></li>
                        <li><a href="<?php echo base_url().'document/';?>">Document</a></li>
                        <li><a href="<?php echo base_url().'person/';?>">Members</a></li>
                    </ul>
                    <div id="tabs-1"></div>
                    <div id="tabs-2"></div>
                    <div id="tabs-3"></div>    
                    <div id="tabs-4"></div>
                    <div id="tabs-5"></div>
                    <div id="tabs-6"></div>
                </div>
                <p>
                    <!--<button type="button" class="btn btn-lg btn-primary" id="btnPrimary">Primary</button>-->
                </p>
            </div>        

        </div>

    </div><!-- /.container -->
    </body>
    <script type="text/javascript">
        $("document").ready(function() {
                $("#btnPrimary").click(function() {
                 alert( "Handler for .click() called." );
             });
         });
    </script>
</html>
