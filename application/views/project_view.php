<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */
?>
<div id="project_dialog" title="New project">
    <form method="post" action="<?php print(site_url());?>/project/insert/<?php print $active_tab;?>">
       <label>Name:</label>
            <input name="name" maxlength="50" size="30">
                <div class="buttons">
                    <input type="submit" value="Save">
                    <a href="#" id="close_project">Close</a>
                </div>
    </form>
</div>

