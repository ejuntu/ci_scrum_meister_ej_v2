<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<script>
    $(function() {
        /*$("#add_task").hide();*/
        $("#hide_add").click(function(){
                $("#add_member").hide();
              });
        $("#show_add").click(function(){
                $("#add_member").show();
              });      
    });
</script>
<button id="show_add">Add new member</button>
<div id="add_member">
    <form action="<?php echo site_url().'person/save';?>" method="post">
    <div><label>Name:</label>
        <input name="name" maxlength="50"></div>
    <div><label>Email:</label>
        <input name="email" maxlength="50"></div>
    <div><label>Password</label>
        <input name="password" maxlength="20" 
               type="password" size="20" placeholder="max 20 chars"></div> 
    <div class="buttons">    
        <input type="submit" value="Save">
        <a href="#" id="hide_add">Close</a>
    </div>
    </form>
</div>
<table>
    <tr><th>Name</th><th>email</th><th></th><th></th>
    </tr>
        <?php
        foreach ($persons as $person) {
            print "<tr><td>$person->name</td>&nbsp<td>$person->email</td><td></td>";
            print "<td> <a href='" . site_url() . "person/delete/" .  $person->id . "'>Delete member</a>&nbsp;";
            print "</td></tr>";
        }
        ?>
</table>
