<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */
?>
<p>Selected sprint:<?php print $sprint_id ?></p>
<table class="list">
    <tr>
        <th class="id"></th>
        <th>Title</th>
        <th>Description</th>
        <th>Assigned</th>
    </tr>
    <?php
    foreach ($tasks as $task) {
        print "<tr>";
        print "<td class='id'>".$task->id."</td>";
        print "<td>".$task->title."</td>";
        print "<td>".$task->description."</td>";
        print "<td class='list_text'>";
        foreach ($task->members as $member) {
            print $member->name."&nbsp;";
        }
        print "</td>";
        print "</tr>";
    }
    ?>
</table>


