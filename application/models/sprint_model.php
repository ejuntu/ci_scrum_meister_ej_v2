<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of sprint_model
 *
 * @author n4jues00
 */
class Sprint_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all($project_id=0) {
        if ($project_id>0) {
            $this->db->where('project_id',$project_id);
        }
        $query=$this->db->get('sprint');
        return $query->result();
    }
    
        public function get($id) {
        $this->db->where('id',$id);
        $query=$this->db->get('sprint');
        return $query->row();
    }
    
    public function save($data) {
        $this->db->insert('sprint',$data);
        return $this->db->insert_id();
    }
    
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('sprint');
    }
    
    public function update($data) {
        $this->db->where('id',$data['id']);
        $query=$this->db->update('sprint',$data);
    }
    
    public function get_first($project_id) {
        $this->db->where('project_id',$project_id);
        $this->db->order_by('id','asc');
        $this->db->limit(1);
        $query=$this->db->get('sprint');
        return $query->row();
    }
}
