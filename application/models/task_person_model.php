<?php

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of task_person_model
 *
 * @author n4jues00
 */
class Task_person_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /* Tässä haetaan taskille nimetyt henkilöt joinilla */
    public function get_all($task_id) {
        $this->db->select('person.name');
        $this->db->from('person');
        $this->db->join('task_person','task_person.person_id=person.id');
        $this->db->where('task_person.task_id = ',$task_id);
        $query=$this->db->get();
        return $query->result();
    }
    
}
