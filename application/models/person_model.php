<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of person_model
 * CREATE TABLE IF NOT EXISTS person (
    id int primary key auto_increment,
    name varchar(100) NOT NULL,
    email varchar(100) NOT NULL UNIQUE,
    password varchar(255) NOT NULL
 *
 * @author n4jues00
 */
class Person_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->model('task_person_model');
        

    }
    public function get_all() {
        $query=$this->db->get('person');
        return $query->result();
    }
    
    public function save($data) {
        $this->db->insert('person',$data);
        return $this->db->insert_id();
    }
  
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('person');
    }
    
    public function update($data) {
        $this->db->where('id',$data['id']);
        $query=$this->db->update('person',$data);
    }
    
    //Find person based on userid and password
    public function get($email, $password) {
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query=$this->db->get('person');
        return $query->row();
        
    }
    
    public function check_user($email,$password) {
        $user=$this->get($email,$password);
        if (!empty($user)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}
