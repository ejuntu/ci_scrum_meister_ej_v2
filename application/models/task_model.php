<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of backlog
 * links to task table in DB
 * CREATE TABLE IF NOT EXISTS task (
    id int primary key auto_increment,
    project_id int not null,
    sprint_id varchar(10), 
    title varchar(50),
    description text,
    priority int,
    CONSTRAINT fk_project_task_id foreign key (project_id) references project(id)
    ON DELETE RESTRICT
 * @author n4jues00
 */
class Task_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all($project_id=0,$sprint_id='') {
        $this->load->model('task_person_model');
        if($project_id>0){
            $this->db->where('project_id',$project_id);
        }
        if(strlen($sprint_id)>0) {
            $this->db->where('sprint_id',$sprint_id);
        }
        $query=$this->db->get('task');
        $tasks=$query->result();
        if (strlen($sprint_id)>0) {
            foreach($tasks as $task) {
                $task->members=$this->task_person_model->get_all($task->id);
                $task->work="";
            }
        }
        return $tasks;
    }
    
    public function save($data) {
        $this->db->insert('task',$data);
        return $this->db->insert_id();
    }
    
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('task');
    }
    
    public function update($data) {
        $this->db->where('id',$data['id']);
        $query=$this->db->update('task',$data);
    }
}