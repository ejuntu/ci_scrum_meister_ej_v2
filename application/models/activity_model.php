<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of activity_model
 *
 * @author n4jues00
 */
class activity_model extends CI_Model {
    
    /*create table if not exists activity (
    id int primary key auto_increment,
    message text not null,
    added timestamp default current_timestamp,
    project_id int not null,
    constraint fk_project_activity_id foreign key (project_id) references project(id)
    on delete restrict,
    person_id int not null,
    constraint fk_person_activity_id foreign key (person_id) references person(id)
    on delete restrict*/
    
    public function __construct() {
        parent::__construct();
    }
    
        public function get_all() {
        $this->db->order_by('added','desc');    
        $query=$this->db->get('activity');
        return $query->result();
    }
    
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('activity');
    }
    
        public function insert($data) {
        $this->db->insert('activity',$data);
        return $this->db->insert_id();
    }
    
    //returns newest activity messages starting $from_id 
    public function get_newest($from_id) {
        $this->db->where('id >',$from_id);
        $query=$this->db->get('activity');
        return $query->result();        
    }
  
}
