<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of MY_controller
 *
 * @author n4jues00
 */
class MY_Controller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('login','refresh');
        }
    }
    
    public function set_project($value) {
        $this->session->set_userdata('project',$value);
    }    
    
    public function get_project() {
        return $this->session->userdata('project');
    }
    
    public function set_sprint($value) {
        $this->session->set_userdata('sprint',$value);
    }
    
    public function get_sprint() {
        return $this->session->userdata('sprint');
    }
    
    public function get_user() {
        return $this->session->userdata('user');
    }  
}
