<?php

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of MY_test_base
 * Tästä voisi tehdä kantaluokan testiluokille
 * on käytettävä vain yhtä core-luokkaa yhdessä aliluokassa
 * @author n4jues00
 */
class MY_test_base extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
}
