<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of login
 *
 * @author n4jues00
 */
class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('person_model');
    }
    
    public function index() {
        $this->load->view('login_view.php');
    }
    
    public function validate() {
        $person=$this->person_model->get($this->input->post('email'),md5($this->input->post('password')));
        
        if (empty($person)) {
            $this->session->set_userdata('active_tab',0);
            $this->index();
        }
        else {
            $this->session->set_userdata('user',$person);
            $this->session->set_userdata('logged_in',true);
            redirect('project/index');
        }
    }
    
    public function logout() {
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login/index');
    }
}
