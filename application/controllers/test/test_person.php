<?php

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of test_person
 * Controllerin testiluokka
 * @author n4jues00
 */
class Test_Person extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model('../controllers/person');
    }
    
        public function index() {
        print "TESTING SCRUM_MEISTER...";
        $this->index_view();
        /*$this->insert_fail_no_name();*/
        print $this->unit->report();
    }
    
    private function index_view() {
        //Asetetaan istuntomuuttujaan "Kirjautuminen"
        /*$this->session->set_userdata("user","dummy_person");*/
        //Tyhjennetään tietokannan taulu
        $this->empty_person_table();
        //Lisätään tekstihenkilö
        $pw=md5("test");
        $sql="insert into person(name,email,password) values ('Test','Person','$pw')";
        $this->execute_sql($sql);
        //Aloitetaan tulostuksen (eli PHP:n tuottaman HTML:n) puskurointi
        ob_start();
        //Ladataan käsittelijä
        $this->load->library("../controllers/person");
        //Kutsutaan käsittelijän index-metodia
        $this->person->index();
        //Luettaan PHP:n tuottama sivu muuttujaan 
        $view=  ob_get_clean();
        //Tarkastetaan löytyykö sisällöstä sana person
        $surname_starts=strpos($view,"Person");
        $surname=substr($view,$surname_starts,6);
        $this->unit->run($surname,"Person","index_view");
    }
    
        private function empty_person_table() {
        $db=mysqli_connect("localhost","root","","scrum_meister");
        mysqli_query($db,"delete from person;");
        mysqli_close($db);
    }
    
        private function execute_sql($sql) {
        $db=  mysqli_connect("localhost", "root", "", "scrum_meister");
        $result=  mysqli_query($db, $sql);
        mysqli_close($db);
        return $result;
    }
}
