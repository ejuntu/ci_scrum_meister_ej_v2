<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of activity
 *
 * @author n4jues00
 */
class Activity extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('activity_model');
        $this->load->model('sprint_model');
        $this->load->model('person_model');
    }
    public function index() {
        $data['activities']=$this->activity_model->get_all();
        $data['users']=$this->person_model->get_all();
        $data['user_id']=$this->session->userdata('user')->id;
        $data['project_id']=$this->get_project()->id;
        $this->load->view('activity_view',$data);
    }
    
    public function insert() {
        //Selvitä person_id postin userista
        $user_id=$this->session->userdata('user')->id;
        $data=array(
            'message' => $this->input->post('message'),
            'project_id'=>$this->input->post('project_id'),
            'person_id'=>$this->input->post('user_id')
        );
        $this->activity_model->insert($data);
        redirect('project/index/','refresh');
    }
    
        public function delete($id) {
        $this->activity_model->delete($id);
        redirect('project/index/','refresh');
    }
    
    public function update_feed() {
        $last_id=$this->input->post('last_id');
        //print "Last id:".$last_id;
        $users=$this->person_model->get_all();
        header('Content-type: application/json');
         //DB-haku
        $activities=$this->activity_model->get_newest($last_id);
        $act_size=count($activities);
        //print "/activities size:".$act_size;
        $json='[';
        foreach ($activities as $activity) {
            $person_id=$activity->person_id;
            $json.="{";
            foreach($users as $user){
                if ($user->id==$person_id) {
                    $user_name=$user->name;
                }
            }
            $json.='"username":"'.$user_name.'",';
            $json.='"added":"'.$activity->added.'",';
            $json.='"id":'.$activity->id.',';
            $json.='"message":"'.$activity->message.'"';
            $json.="},";
        }
        $json=  substr($json, 0,  strlen($json)-1);
        //$json.='{"username":"user"}';
        $json.=']';
        print $json;        
    
    }
}