<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of task
 ** CREATE TABLE IF NOT EXISTS task (
    id int primary key auto_increment,
    project_id int not null,
    sprint_id varchar(10), 
    title varchar(50),
    description text,
    priority int,
    CONSTRAINT fk_project_task_id foreign key (project_id) references project(id)
    ON DELETE RESTRICT
 * @author n4jues00
 */
class Backlog extends My_Controller{
     public function __construct() {
        parent::__construct();
        $this->load->model('task_model');
    }

    public function index() {
        $data['tasks']=$this->task_model->get_all();
        $this->load->view('backlog_view',$data);
    }
    
    public function add() {
        $data['header']="Scrum Meister - add a task";
        $this->load->view('backlog_view',$data);
    }
    
    public function save() {
        $data=array(
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'project_id' => 1,

        );
        $this->task_model->save($data);
        redirect('project/index/1','refresh');
    }
    
    public function update() {
        $data=array(
            'id'=>$this->input->post('id'),
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'project_id' => 1,

        );
        $task_id=$this->task_model->update($data);
        redirect('project/index/1','refresh');
    }
    
    public function delete($id) {
        $this->task_model->delete($id);
        redirect('project/index/1','refresh');
    }
}
