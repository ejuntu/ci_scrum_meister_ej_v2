<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of person Controller
 *
 * @author n4jues00
 */
class Person extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('person_model');
    } 
    
    public function index() {
        $data['persons']=$this->person_model->get_all();
        $this->load->view('persons_view',$data);
    }
    
    public function add() {
        $data['header']="Scrum Meister - add a person";
        $this->load->view('person_view',$data);

    }
    
    public function save() {
        $data=array(
            'name' => $this->input->post('name'),
            'email'=>$this->input->post('email'),
            'password'=>md5($this->input->post('password'))
        );
        $this->person_model->save($data);
        redirect('project/index/','refresh');
    }
    
    public function delete($id) {
        $this->person_model->delete($id);
        redirect('project/index/','refresh');
    }
}
