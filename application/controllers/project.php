<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of project
 *
 * @author jjuntune, Esa
 */
class Project extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sprint_model');
    }    
    
    public function index($active_tab=0) {
        $data['header']="Scrum Meister Index";
        if($this->session->userdata("active_tab")==TRUE) {
            $data['active_tab']=$this->session->userdata('active_tab');
        }
        else {
            $data['active_tab']=0;
        }
                // If application is just opened retrieve first project and it's first sprint.
        if ($this->get_project()==FALSE) {            
            $this->set_project($this->project_model->get_first());            
        }        
        
        // Check if there are no projects into the database.
        if ($this->get_project()!=FALSE) {
            $this->set_sprint($this->sprint_model->get_first($this->get_project()->id));            
            $data['project']=$this->get_project();
            $data['sprint']=$this->get_sprint();        
            $data['projects']=$this->project_model->get_all();        
            $data['sprints']=$this->sprint_model->get_all($this->get_project()->id);
        }
        else {            
            $data['project']=NULL;
            $data['sprint']=NULL;        
            $data['projects']=NULL;        
            $data['sprints']=NULL;
        }
        
        
        $data['user']=$this->get_user();
        
        $this->load->view('template',$data);
    }
    
   public function insert() {
        $data=array(
            'name' => $this->input->post('name')
        );
        $this->project_model->save($data);
        $this->index();
    }
    
    public function delete($id) {
        $this->project_model->delete($id);
        $this->index();
    }
    
    public function change_project($project_id) {
        $this->set_project($this->project_model->get($project_id));
        $this->set_sprint($this->sprint_model->get_first($this->get_project()->id));            
        $this->index();
    }
    
    public function change_sprint($sprint_id) {
        $this->set_sprint($this->sprint_model->get($sprint_id));            
        $this->index();
    }
    
    public function json_test(){
         $this->load->view('json_test');
        
    }
}












