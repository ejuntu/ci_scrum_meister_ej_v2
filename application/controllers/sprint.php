<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OAMK Web-arkkitehtuurit k. 2015
 * Esa Juntura
 */

/**
 * Description of sprint
 *
 * @author n4jues00
 */
class Sprint extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sprint_model');
        $this->load->model('task_model');
        $this->load->model('task_person_model');
        $this->load->model('person_model');
        $this->load->model('work_model');
    }
     public function index() {
        $data['sprint_id']='I';
        $data['tasks']=$this->task_model->get_all(1,'I');
        $data['header']="Scrum Meister - sprints";
        /*$this->load->view('sprints_view',$data);*/
        $this->load->view('sprint_plan_view',$data);
    }
    
    public function add() {
        $data['header']="Scrum Meister - add a sprint";
        $this->load->view('sprint_view',$data);

    }
    
    public function save() {
        $data=array(
            'sprint_id' => $this->input->post('sprint_id'),
            'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'project_id' => $this->input->post('project_id')
        );
        
        $this->sprint_model->save($data);
        $this->index();
    }
    
    public function delete($id) {
        $this->sprint_model->delete($id);
        $this->index();
    }
}
